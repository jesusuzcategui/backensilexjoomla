<?php

require "./vendor/autoload.php";
use \Dompdf\Dompdf;
use \Dompdf\Options;

$pathProject = dirname(dirname(__FILE__));

date_default_timezone_set('America/Bogota');

define( '_JEXEC', 1 );
define( 'DS', DIRECTORY_SEPARATOR );
define( 'JPATH_BASE', $pathProject );

/**AUTOLOAD */
require_once(JPATH_BASE . DS . 'api' . DS . 'autoload.php');
/**AUTOLOAD */

require_once( JPATH_BASE . DS . 'includes' . DS . 'defines.php' );
require_once( JPATH_BASE . DS . 'includes' . DS . 'framework.php' );
require_once( JPATH_BASE . DS . 'libraries' . DS . 'joomla' . DS . 'database' . DS . 'factory.php' );

use Joomla\CMS\Filesystem\File;
use Joomla\CMS\Http\HttpFactory;
use Medoo\Medoo;

use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\HttpFoundation\Request;

$Joomla  = JFactory::getApplication('site');
$dbo     = JFactory::getDbo();
$session = JFactory::getSession();
$httpFac = HttpFactory::getHttp(null, ['curl', 'stream']);
$user    = JFactory::getUser();

/**CONEXION CON DB**/
$database = new Medoo([
	// [required]
	'type' => 'pgsql',
	'host' => 'localhost',
	'database' => 'extendsystem',
	'username' => 'postgres',
	'password' => 'Tumble2021.',
 
	// [optional]
	'charset' => 'utf8mb4',
	'collation' => 'utf8mb4_general_ci',
	'port' => 5432,

    'prefix' => 'sasa_'
    
]);


$request = Request::createFromGlobals();
$idevento = $request->query->get('idevento');

$allentriesByEventQUERY = <<<SQL
select 
* 
from sasa_match_modules_participations as parti
where parti.event_id = '{$idevento}';
SQL;

$allentriesByEventRs = $database->pdo->prepare($allentriesByEventQUERY);
$allentriesByEventRs->execute();
$allentriesByEventResult = $allentriesByEventRs->fetchAll(\PDO::FETCH_ASSOC);

//var_dump($allentriesByEventResult);

ob_start();
echo "<style>
table {
	border-collapse: collapse;
	font-size: 10px;
	width: 100%;	
}

table tr td {
	border: solid 1px #000;
}
</style>";
if(count($allentriesByEventResult) > 0) {
    foreach($allentriesByEventResult as $i => $entry) {
        require('./docs_template/evaluate.php');
    }
}

$html = ob_get_clean();

try	{
	echo $html;
	/*header("Accept: application/pdf");
	header("Content-Type: application/pdf; charset=UTF-8");
	// instantiate and use the dompdf class
	$dompdfOptions = new Options;
	$dompdfOptions->set('isRemoteEnabled', true);

	$dompdf = new Dompdf($dompdfOptions);
	$dompdf->loadHtml($html);

	// (Optional) Setup the paper size and orientation
	$dompdf->setPaper('LETTER', 'portrait');
	$dompdf->isRemoteEnabled = true;

	// Render the HTML as PDF
	$dompdf->render();

	// Output the generated PDF to Browser
	$dompdf->stream('DOCUMENTO', array("Attachment" => false));*/
} catch(\Exception $e) {
	var_dump($e->getMessage());
}