<?php

$judges = <<<SQL
select 
usr.*
from sasa_match_core_panels as panel 
left join sasa_match_core_panels_events as pevent 
on pevent.panel = panel.id 
left join sasa_match_core_panels_categories as pcat 
on pcat.panelid = panel.id 
left join dblink('dbname=extendsystem',
				 'select par.id, par.category_id from sasa_match_modules_participations AS par') as parti(id uuid, category_id bigint)
on parti.category_id = pcat.categoryid 
cross join json_array_elements(panel.generaljudge) as each_gen
left join dblink('dbname=extendsystem',
				 'select usr.serial, usr.id, usr.name, usr.email from sasa_match_users AS usr') as usr(serial uuid, id bigint, name text, email text)
on concat(usr.id::text, '__', usr.serial::text) = each_gen->>'general'
where pcat.categoryid = '{$entry["category_id"]}' 
and pevent.event = '{$entry["event_id"]}' 
and parti.id = '{$entry["id"]}'
SQL;

$judgesPenal = <<<SQL
select 
usr.*
from sasa_match_core_panels as panel 
left join sasa_match_core_panels_events as pevent 
on pevent.panel = panel.id 
left join sasa_match_core_panels_categories as pcat 
on pcat.panelid = panel.id 
left join dblink('dbname=extendsystem',
				 'select par.id, par.category_id from sasa_match_modules_participations AS par') as parti(id uuid, category_id bigint)
on parti.category_id = pcat.categoryid 
cross join json_array_elements(panel.penaltyjudge) as each_gen
left join dblink('dbname=extendsystem',
				 'select usr.serial, usr.id, usr.name, usr.email from sasa_match_users AS usr') as usr(serial uuid, id bigint, name text, email text)
on concat(usr.id::text, '__', usr.serial::text) = each_gen->>'penalty'
where pcat.categoryid = '{$entry["category_id"]}' 
and pevent.event = '{$entry["event_id"]}' 
and parti.id = '{$entry["id"]}'
SQL;

$sqlNameEvent = <<<SQL
select title from sasa_content where id = '{$entry["event_id"]}';
SQL;

$sqlNameCategory = <<<SQL
select title from sasa_match_core_categories where id = '{$entry["category_id"]}';
SQL;

$sqlNameCompetitor = <<<SQL
select comp.data->>'name' as teamname from sasa_match_modules_participations as parti
left join sasa_match_competitor as comp on comp.id = parti.partipant_id 
where parti.id = '{$entry["id"]}';
SQL;

$nameEvent = $dbo->setQuery($sqlNameEvent)->loadResult();
$nameCategory = $dbo->setQuery($sqlNameCategory)->loadResult();
$nameCompetitorTS = $database->pdo->prepare($sqlNameCompetitor);
$nameCompetitorTS->execute();
$nameCompetitorTSResult = $nameCompetitorTS->fetchColumn();

$judgesOfEntry = $dbo->setQuery($judges)->loadAssocList(); 
$judgesPenalOfEntry = $dbo->setQuery($judgesPenal)->loadAssocList(); 
$alljud  = array();
array_walk($judgesOfEntry, function($e) use (&$alljud) {
    array_push($alljud, $e);
});

array_walk($judgesPenalOfEntry, function($e) use (&$alljud) {
    array_push($alljud, $e);
});

foreach($alljud as $j => $judge) {    
    /******************************/
    /********PARAMETROS************/
    /******************************/
    $mysgeneralparamsSQL = <<<SQL
    select 
    each_general->>'id' as contenedor,
    parameterofi.id as parametro_id,
    parameterofi.title as parametro_name,
    parameterofi.description as parametro_desc,
    parameterofi.criteria as parametro_criteria
    from sasa_match_core_panels_categories as panelcat 
    left join sasa_match_core_panels as panel
    on panel.id = panelcat.panelid
    cross join json_array_elements(panel.generaljudge) as each_general
    cross join json_array_elements(each_general->'params') as each_params
    left join dblink('dbname=extendsystem', 'select par.id, par.category_id from sasa_match_modules_participations AS par') 
                    as parti(id uuid, category_id bigint) on parti.category_id = panelcat.categoryid
    left join sasa_match_core_categories_parameters as catpar
    on catpar.categoria = panelcat.categoryid
    left join sasa_match_core_parameters as parameterofi
    on parameterofi.id = catpar.parametro
    and parameterofi.id = (each_params->>'id')::int
    left join sasa_match_core_panels_events as pevent 
    on pevent.panel = panel.id
    where 
    (each_general->>'general') like '%{$judge["serial"]}'
    and pevent.event = '{$entry["event_id"]}'
    and parti.id = '{$entry["id"]}'
    and parameterofi.id is not null
    group by parameterofi.id, each_general->>'id'
    SQL;
    
    $resultMyParams = $dbo->setQuery($mysgeneralparamsSQL)->loadAssocList();

    /******************************/
    /********PENALTY************/
    /******************************/
    
    $myspenaltySQL = <<<SQL
    SELECT
    parpenal.id AS penalty_id,
    parpenal.title AS penalty_name,
    parpenal.data->>'desc' AS penalty_desc,
    parpenal.points AS penalty_point
    FROM sasa_match_core_panels_categories AS panelcat
    LEFT JOIN sasa_match_core_panels AS panel ON panel.id = panelcat.panelid
    CROSS JOIN json_array_elements(panel.penaltyjudge) AS each_penal
    CROSS JOIN json_array_elements(each_penal->'params') AS each_params
    LEFT JOIN dblink('dbname=extendsystem', 'select par.id, par.category_id from sasa_match_modules_participations AS par') 
    AS parti(id UUID, category_id bigint) ON parti.category_id = panelcat.categoryid
    LEFT JOIN sasa_match_core_panels_events AS pevent ON pevent.panel = panel.id
    LEFT JOIN sasa_match_core_parameters_penalty AS parpenal ON parpenal.id::int = (each_params->>'id')::int
    WHERE parti.id = '{$entry["id"]}' 
    AND (each_penal->>'penalty') like '%{$judge["serial"]}'
    AND pevent.event = '{$entry["event_id"]}'
    AND parpenal.id IS NOT NULL
    SQL;

    $resultMyPenals = $dbo->setQuery($myspenaltySQL)->loadAssocList();

    if(count($resultMyParams) > 0){
        include('./docs_template/evaluate_primary.php');
    }

    if(count($resultMyPenals) > 0){
        include('./docs_template/evaluate_penalty.php');
    }


}


?>