<table style="width: 100%; font-family: sans-serif;">
    <tr>
        <td>
            <img style="width: 100px" src="https://systemapi.tumbleusa.us/api/Images/tumbleusa_eventsb_250px.png" />
        </td>
        <td>
            <h2>TUMBLEUSA JUDGING SYSTEM</h2>
            <p><?php echo $nameEvent; ?></p>
        </td>
    </tr>  
    <tr>
        <td colspan="2" valign="top">
            <table>
                <tr>
                    <td>GENERAL JUDGE TEMPLATE</td>
                </tr>
                <tr>
                    <td><strong>DATE</strong></td>
                    <td><?php echo date('y/m/j'); ?></td>
                    <td><strong>TIME</strong></td>
                    <td><?php echo date('h:i:s a'); ?></td>
                    <td><strong>VERSION</strong></td>
                    <td>1</td>
                </tr>
                <tr>
                    <td colspan="2"><strong>JUDGE NAME</strong></td>
                    <td colspan="4"><?php echo $judge['name']; ?></td>
                </tr>
                <tr class="row8">
                    <td class="column0 style12 s">CATEGORY:</td>
                    <td class="column1 style13 s style11" colspan="2"><?php echo $nameCategory; ?></td>
                    <td class="column3 style14 s">COMPETITOR:</td>
                    <td class="column4 style13 s style11" colspan="2"><?php echo $nameCompetitorTSResult; ?></td>
                    <td class="column6 style13 s style11" colspan="2">TEAM OR SINGLE</td>
                </tr>
                <tr class="row9">
                    <td class="column0 style12 s">JUDGE KIND:</td>
                    <td class="column1 style13 s style11" colspan="2">GENERAL</td>
                    <td class="column3 style15 s"></td>
                    <td class="column4 style13 s style11" colspan="2">JUDGE ID</td>
                    <td class="column6 style13 s style11" colspan="2"><?php echo $entry['id']; ?></td>
                </tr>
                <tr class="row11">
                    <td class="column0 style16 s style0" colspan="8">GENERAL JUDGE TEMPLATE</td>
                </tr>
                <tr style="background-color: #000; color: #fff;" class="row12">
                    <td class="column0 style17 s style11" colspan="2">PARAMETER NAME</td>
                    <td class="column2 style13 s style10" colspan="4">DESCRIPTION</td>
                    <td class="column6 style13 s style11" colspan="2">TOTAL POSIBLE SCORE</td>
                </tr>
                <?php foreach($resultMyParams as $l => $param): ?>
                    <?php 
                    $criteriaDecode = json_decode($param['parametro_criteria']);
                    $totalParam = 0;
                    foreach($criteriaDecode as $crit){
                        $totalParam = $totalParam + doubleval($crit->points); 
                    }
                    ?>
                    <tr style="background-color: #2d2d2d; color: #fff;" class="row12">
                        <td class="column0 style17 s style11" colspan="2"><?php echo $param['parametro_name']; ?></td>
                        <td class="column2 style13 s style10" colspan="4"><?php echo $param['parametro_desc']; ?></td>
                        <td class="column6 style13 s style11" colspan="2"><?php echo $totalParam; ?></td>
                    </tr>
                    <tr style="background-color: #000; color: #fff;" class="row12">
                        <td class="column0 style17 s style11" colspan="2">CRITERIA</td>
                        <td class="column2 style13 s style10" colspan="4">POSSIBLE POINTS</td>
                        <td class="column6 style13 s style11" colspan="2">POINTS</td>
                    </tr>
                    <?php
                        foreach($criteriaDecode as $crit):
                    ?>
                        <tr>
                            <td colspan="2"><?php echo $crit->title; ?></td>
                            <td colspan="4"><?php echo $crit->points; ?></td>
                            <td colspan="2" style="height: 30px;">&nbsp;</td>
                        </tr>
                    <?php endforeach; ?>
                <?php endforeach; ?>
            </table>
        </td>
    </tr>
</table>

<div style="page-break-after:always;"></div>