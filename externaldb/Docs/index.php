<?php 

use \Dompdf\Dompdf;

$docs->get('/masivedocs/{entry}', function($entry) use ($dbo, $database){
    header("Accept: application/pdf");
    header("Content-Type: application/pdf; charset=UTF-8");
    // instantiate and use the dompdf class
    $dompdf = new Dompdf();
    $dompdf->loadHtml('hello world');

    // (Optional) Setup the paper size and orientation
    $dompdf->setPaper('LETTER', 'landscape');

    // Render the HTML as PDF
    $dompdf->render();

    // Output the generated PDF to Browser
    $dompdf->stream('DOCUMENTO', array("Attachment" => false));
    return "1";
});


?>